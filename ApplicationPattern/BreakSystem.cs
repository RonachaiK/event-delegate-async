﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public class BreakSystem
    {
        private readonly Car _car;

        public BreakSystem(Car car)
        {
            this._car = car;
        }

        public delegate void  BreakHandler(object source, BreakEventArgs args);
        public event  BreakHandler Breaking;
        public event BreakHandler Breaked;

        public void Break()
        {
            OnBreaking();
            OnBreaked();
        }
        protected virtual void OnBreaking()
        {
            if (Breaking != null)
            {

                Breaking(this, new BreakEventArgs() { Vehicle = _car });
                Console.WriteLine("System Breaking....");
              
            }
        }
        protected virtual void OnBreaked()
        {
            if (Breaked != null)
            {
                Breaked(this, new BreakEventArgs() { Vehicle = _car });
                Console.WriteLine("Stoped....\n");
            }
        }
      

    }

}
