﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public abstract class Vehicle
    {
        public abstract IVehicle CreateVehicle(VehicleType.Type type);

    }
}
