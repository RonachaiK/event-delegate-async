﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public class BreakEventArgs : EventArgs
    {
        public IVehicle Vehicle { get; set; }
    }

}
