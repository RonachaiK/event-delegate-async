﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public class Car : IVehicle
    {
        public string Brand { get; set; }

        public void Accelerate()
        {
            Console.WriteLine("\nCar " + this.Brand + " is Accelerating...");
            Console.WriteLine("Press \"b\" to break....");

            if (Console.ReadKey().KeyChar == 'b')
            {
                Console.WriteLine();
                Break();
                Console.WriteLine("Press \"a\" to Accelerate....");
                if (Console.ReadKey().KeyChar == 'a')
                    Accelerate();

            }
            Accelerate();
        }

        public void Break()
        {
            var breakSystem = new BreakSystem(this);
            var breakService = new BreakService(breakSystem);
            breakSystem.Break();

        }



    }
}
