﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public class BreakService
    {
        private readonly  BreakSystem _breakSystem;
        public BreakService(BreakSystem breakSystem)
        {
            _breakSystem = breakSystem;
            _breakSystem.Breaking += this.OnBreaking;
            _breakSystem.Breaked += this.OnBreaked;
        }
        protected void OnBreaking(object obj, BreakEventArgs e)
        {
            Console.WriteLine("Car " + e.Vehicle.Brand + " Break System is breaking...");
            IAsyncResult asyncResult =  BreakWaiting();
            while (!asyncResult.IsCompleted)
            {
                asyncResult.AsyncWaitHandle.WaitOne(1000);
                //do something while car is breaking
                Console.WriteLine("Waiting for Car is stopped!!");
            }
        }
        private async Task<Task<bool>> BreakWaiting()
        {
            await Task.Delay(3000);
            return Task.FromResult<bool>(true);
        }

        protected void OnBreaked(object obj, BreakEventArgs e)
        {
            Console.WriteLine("Car " + e.Vehicle.Brand + " Break System is breaked...");
            
        }
    }

}
