﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    public class VehicleCreator : Vehicle
    {
        public override IVehicle CreateVehicle(VehicleType.Type type)
        {
            switch (type)
            {
                case VehicleType.Type.Car: return new Car();
                default: throw new ArgumentException("Invalid type", type.ToString());
            }
        }
    }
}
