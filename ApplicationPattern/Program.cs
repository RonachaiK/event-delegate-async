﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var vehicle = new VehicleCreator();
            var car = vehicle.CreateVehicle(VehicleType.Type.Car);
            car.Brand = "Toyota";
            car.Accelerate();

        }

    }
   
}
